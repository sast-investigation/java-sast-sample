# SAST Analyzer Java sample
| 起動  | SAST Analyzer                                                                           | ジョブ名            | スキャナー                                                                                                                                            |
|-----|-----------------------------------------------------------------------------------------|-----------------|--------------------------------------------------------------------------------------------------------------------------------------------------|
| 無効  | [spotbugs analyzer](https://gitlab.com/gitlab-org/security-products/analyzers/spotbugs) | `spotbugs-sast` | [Find Security Bugs](https://find-sec-bugs.github.io/) ([検出ルール](https://find-sec-bugs.github.io/bugs_ja.htm))                                    |
| 有効  | [Semgrep analyzer](https://gitlab.com/gitlab-org/security-products/analyzers/semgrep)   | `semgrep-sast`  | [Semgrep](https://semgrep.dev/) ([検出ルール](https://gitlab.com/gitlab-org/security-products/analyzers/semgrep/-/blob/main/rules/find_sec_bugs.yml)) |

SAST で検査した結果のレポートは[パイプライン](https://gitlab.com/sast-investigation/java-sast-sample/-/pipelines)からダウンロードしてください。


## サンプルファイル
* [Vulnerability.java](./src/main/java/Vulnerability.java) - コマンドインジェクションのあるコード。
* [Ignore.java](./src/main/java/Ignore.java) - 検出された脆弱性の無視。

## その他
### spotbugs analyzer
* 静的解析エンジンに [SpotBugs](https://spotbugs.github.io/) を使っていますが、検出ルールは [Find Security Bugs](https://find-sec-bugs.github.io/) だけを利用しています。
* Find Security Bugs 自体は JSP にも対応していますが、spotbugs analyzer では[非対応](https://gitlab.com/gitlab-org/gitlab/-/issues/210498)です。
* Kotlin / Scala などにも対応しています。
* Java プロジェクトでは標準では起動しません。(Kotlin / Scala などでは起動します)
* コンパイルされたクラスファイルを解析しているようです。


### Semgrep anlyzer
* Find Security Bugs の検出ルールをもとに、GitLab 社が Semgrep ルールに移植したものです。
* Java 以外の言語には非対応です。
* Semgrep の仕様により、Find Security Bugs から[移植されていない検出ルール](https://gitlab.com/gitlab-org/secure/gsoc-sast-vulnerability-rules/playground/sast-rules#find-sec-bugs)があります。
