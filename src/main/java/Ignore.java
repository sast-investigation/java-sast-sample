import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;

import java.io.IOException;

public final class Ignore {
    private static final Runtime runtime = Runtime.getRuntime();

    @SuppressFBWarnings()
    public static void findbugs(String command) throws IOException {
        runtime.exec(command);
    }

    public static void semgrep(String command) throws IOException {
        runtime.exec(command); // nosemgrep: find_sec_bugs.COMMAND_INJECTION-1
    }
}
